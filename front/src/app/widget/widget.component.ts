import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service'
import { interval } from 'rxjs';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css']
})
export class WidgetComponent implements OnInit {
  state: string = "waiting"

  constructor(private callService: CallService) { }
  validator = /(^[0-9]{9}$)/
  ngOnInit() {
  }
    number: string
    call(){
  
      if(this.isValidNumber()){
        this.callService.placeCall(this.number)
        this.state = "ringing"
        this.callService.getCallId().subscribe(callId => {
          this.checkStatus()         
        })
      }  else {
          console.info("Numer niepoprawny")
      } 
    }
    checkStatus() {
     // state === "ANSWERED" || state === "FAILED" ||
     // state === "NO ANSWER" || state === "BUSY"
      this.callService.getCallStatus().subscribe(state => {
        this.state = state
        })
      }

      
    isValidNumber(): Boolean {
     return this.validator.test(this.number);
  }
 
 
}
